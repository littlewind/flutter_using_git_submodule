import 'package:elearning/main.dart';
import 'package:elearning/provider/app_providers.dart';
import 'package:elearning/repository/sql_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_git_dependency/main.dart' as git_depend;

void main() async {
  SqfliteRepository sqlRepo = new SqfliteRepository();
  await sqlRepo.initDb();
  AppProvider().init();
  runApp(git_depend.MyApp());
}
